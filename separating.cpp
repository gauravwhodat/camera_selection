#include<iostream>
#include<fstream>
#include<sstream>
#include<vector>
#include<string>
#include<unordered_map>
#include<iterator>
#include<cmath>
#include<algorithm>
#include<iomanip>

using namespace std;

int constexpr noisy_cameras = 0;
int constexpr nil = -1;
float radius_range = 0.00025;
float increment_in_radius_range = 0.00005;
float increment_in_angle = 10.0; //on;y required if range goes out of bound

float angle_error = 20; //in degrees

float DEG_TO_RAD = 0.017453292519943295769236907684886f;
float RAD_TO_DEG = 57.2958;

void error(string s)
{
  throw runtime_error (s);
}


class Mat{
public:

	int rows;
	int cols;
	vector<vector<float>> element;
	Mat(int no_of_rows, int no_of_cols):rows(no_of_rows), cols(no_of_cols)
	{	
		element.resize(no_of_rows);
		for(int i = 0; i<no_of_rows; i++)
		{
			element[i].resize(no_of_cols);
			for(int j = 0; j<no_of_cols;j++)
					element[i][j] = 0;
		}
	}

    Mat tr()//transpose
	{
		Mat transposed_matrix(cols,rows);
		for(int i = 0; i<rows; i++)
		for(int j = 0; j<cols; j++)
			transposed_matrix.element[j][i] = element[i][j];
		return transposed_matrix;
	}
};

class Vector3d{
public:

	float x;
	float y;
	float z;
	//vector<vector<float>> elements;
	Vector3d()
	{	
		x = 0; y = 0; z = 0;
	}
};

Mat operator*(Mat const& m1, Mat const& m2)
{
	Mat m(m1.rows, m2.cols);
	if(m1.cols != m2.rows)
		error("Error::Matrices cannot be multiplied");
	for(int i =0; i<m.rows;i++)
		for(int j = 0; j<m.cols;j++)
			{
				float sum = 0;
				for(int k = 0; k<m1.cols; k++)
					sum = sum + m1.element[i][k]*m2.element[k][j];
				m.element[i][j] = sum;
			}
	return m;
}

void eulerFromMat(Mat m, float &heading, float &attitude, float &bank)
{
    // Assuming the angles are in radians.
    if (m.element[1][0] > 0.998)
    { // singularity at north pole
        heading = std::atan2(m.element[0][2], m.element[2][2]);
        attitude = DEG_TO_RAD * 90;
        bank = 0;
        return;
    }
    if (m.element[1][0] < -0.998)
    { // singularity at south pole
        heading = std::atan2(m.element[0][2], m.element[2][2]);
        attitude = -DEG_TO_RAD * 90;
        bank = 0;
        return;
    }
    heading = std::atan2(-m.element[2][0], m.element[0][0]);
    bank = std::atan2(-m.element[1][2], m.element[1][1]);
    attitude = std::asin(m.element[1][0]);
}


void writetofile(ofstream& outfile, vector<float> v)
{
    for(int i = 0; i<v.size();i++)
        outfile<<v[i]<<" ";
    outfile<<endl;
}

void writetofile(ofstream& outfile, Vector3d v)
{
    outfile<<v.x<<" "<<v.y<<" "<<v.z;
    outfile<<endl;
}

void writetofile(ofstream& outfile, Mat m)
{
    for(int i=0; i<m.rows; i++)
    {
        for(int j=0; j<m.cols; j++)
        {
            outfile<<m.element[i][j]<<" ";
        }
        outfile<<endl;
    }
}

void writetofile(ofstream& outfile, vector<vector<float> > v)
{
    for(int i = 0; i<v.size();i++)
        for(int j = 0; j<v[i].size();j++)
            outfile<<v[i][j]<<" ";
    outfile<<endl;
}

Vector3d read_point(ifstream& infile)
{
    Vector3d p;
    string line;
    getline(infile, line);
    istringstream iss(line);
    iss >> p.x >> p.y >> p.z;
    return p;
}

Mat read_rot(ifstream& infile)
{
    Mat out(3,3);
    int cnt = 0;
    for(int i = 0; i<3; i++)
    {
        string line;
        getline(infile, line);
        istringstream iss(line);
        string word;
        while(iss>>word)
        {
            stringstream num(word);
            float x;
            num>>x;
            int temprow = cnt / 3;
            int tempcol = cnt % 3;
            out.element[temprow][tempcol] = x;
            cnt++;
        }
    }
    return out;
}

vector<float> read_f(ifstream& infile)
{
    vector<float> pos;
    string line;
    getline(infile, line);
    istringstream iss(line);
    string word;
    while(iss>>word)
    {
        stringstream num(word);
        float x;
        num>>x;
        pos.push_back(x);
    }
    return pos;
}

vector<float> read_position(ifstream& infile)
{
    vector<float> pos;
    string line;
    getline(infile, line);
    istringstream iss(line);
    string word;
    while(iss>>word)
    {
        stringstream num(word);
        float x;
        num>>x;
        pos.push_back(x);
    }
    return pos;

}

vector<float> read_RGB(ifstream& infile)
{
    vector<float> rgb;
    string line;
    getline(infile, line);
    istringstream iss(line);
    string word;
    while(iss>>word)
    {
        stringstream num(word);
        float x;
        num>>x;
        rgb.push_back(x);
    }
    return rgb;
}


void read_list_of_views(unordered_map<int,int> index, ifstream& infile, vector< vector<float> >& info)
{
    string line;
    getline(infile, line);
    istringstream iss(line);
    string word;
    iss>>word;
    stringstream num(word);
    int len;
    num>>len;

    for(int i = 0; i<len;i++)
    {
        vector<float>vec;
        iss>>word;///reading camera number
        stringstream camindex(word);
        int cam_index;
        camindex>>cam_index;
        //cout<<cam_index<<" "<<index[cam_index]<<endl;
        //auto it = index.find(cam_index);
        //if(it != index.end() && index[cam_index]!=0)
        if(index[cam_index] != nil)
        {
            //cout<<cam_index<<" "<<index[cam_index]<<endl;
            vec.push_back(float(index[cam_index]));
            for(int j = 0;j<3;j++)
            {
                iss>>word;///reading camera number
                stringstream num(word);
                float pos_info;
                num>>pos_info;
                vec.push_back(pos_info);
            }
            info.push_back(vec);
        }
        else
        {
            for(int j = 0;j<3;j++)
                iss>>word;///simply reading the other numbers
        }

    }
}

int select_points(int N, int M, unordered_map<int,int> index, ifstream& infile, ofstream& outfile, bool required_range)
{
    int total_points=0;
    for(int point = 0; point<M; point++)
    {
        vector<float> pos,rgb;
        vector< vector<float> > info;
        pos = read_position(infile);
        rgb = read_RGB(infile);
        read_list_of_views(index,infile,info);
        int len = info.size();
        //cout<<len<<endl;
        if(len>noisy_cameras)
        {
            total_points++;
            if(required_range)
            {
                writetofile(outfile,pos);
                writetofile(outfile,rgb);
                outfile<<len<<" ";
                writetofile(outfile, info);
            }
        }
    }
    return total_points;

}

void compare_and_update(float range, int camera_num, vector<float> f, Vector3d P, Mat M, vector<Vector3d>& position, vector<Mat>& rot, unordered_map<int,int>& index, ofstream& outfile, bool required_range)
{
    if(!position.empty())
    {
        // cout<<camera_num<<" "<<position.size()<<endl;
        for(int i = position.size()-1; i>=0; i--)// in reverse order
        {
            //cout<<rot[i].at<float>(0,0)<<endl;
            //cout<<"Updating "<<camera_num<<" and "<<i<<endl;
            Mat m = M.tr();
            Vector3d P1;
            P1.x = m.element[0][0] * P.x + m.element[0][1] * P.y + m.element[0][2] * P.z;
            P1.y = m.element[1][0] * P.x + m.element[1][1] * P.y + m.element[1][2] * P.z;
            P1.z = m.element[2][0] * P.x + m.element[2][1] * P.y + m.element[2][2] * P.z;

            Mat r = rot[i].tr();
            Vector3d P2;
            P2.x = r.element[0][0] * position[i].x + r.element[0][1] * position[i].y + r.element[0][2] * position[i].z;
            P2.y = r.element[1][0] * position[i].x + r.element[1][1] * position[i].y + r.element[1][2] * position[i].z;
            P2.z = r.element[2][0] * position[i].x + r.element[2][1] * position[i].y + r.element[2][2] * position[i].z;

            if( ( (P1.x-P2.x)*(P1.x-P2.x) + (P1.y-P2.y)*(P1.y-P2.y) + (P1.z-P2.z)*(P1.z-P2.z) ) < range) //P - position[i] < range
            {
                float alpha=0, beta=0, gamma=0;
                Mat x = M.tr() * rot[i];
                //cout<<alpha<<" "<<beta<<" "<<gamma<<endl;
                eulerFromMat(x, alpha, beta, gamma); //alpha, beta and gamma are in radians here
                alpha = RAD_TO_DEG * alpha; beta = RAD_TO_DEG * beta; gamma = RAD_TO_DEG * gamma; ///now converted to degree
                
                if(abs(alpha) > angle_error || abs(beta) > angle_error || abs(gamma) > angle_error)
                {
                    index[camera_num] = position.size();
                    //cout<<camera_num<<" "<<index[camera_num]<<endl;
                    position.push_back(P);
                    rot.push_back(M);
                    
                    if(required_range)
                    {
                        writetofile(outfile, f);
                        writetofile(outfile, M);
                        writetofile(outfile, position[position.size() - 1]);
                    }

                    //cout<<index[camera_num]<<" "<<alpha<<" "<<beta<<" "<<gamma<<endl;
                    return;
                }
                else
                {
                    index[camera_num] = nil;
                    //cout<<camera_num<<" "<<index[camera_num]<<endl;
                    return;
                }
                    
            }
        }
    }
    index[camera_num] = position.size();
    //cout<<camera_num<<" "<<index[camera_num]<<endl;
    position.push_back(P);
    rot.push_back(M);
    if(required_range)
    {
        writetofile(outfile, f);
        writetofile(outfile, M);
        writetofile(outfile, position[position.size()-1]);
    }
    //cout<<index[camera_num]<<endl;
    return;
}

void select_cam(float range, unordered_map<int,int>& index, vector<Vector3d>& position, vector<Mat>& rot, int N, ifstream& infile, ofstream& outfile, bool required_range)
{
    //cout<<N<<endl;
    for(int camera = 0; camera<N; camera++)
    {
        vector<float> f; 
        f = read_f(infile);

        Mat M(3,3);
        M = read_rot(infile);

        Vector3d P;
        P = read_point(infile);
        // cout<<P.x<<" "<<P.y<<" "<<P.z<<endl;

        compare_and_update(range, camera, f, P, M, position, rot, index, outfile, required_range);
    }
}




int main(int argc, char* argv[])
{
    if(argc != 6)
        error("Please enter 5 inputs in the following form: input_file   output_file   percentage-cam   path   outputfile");
    ifstream bundle (argv[1]);    
    ofstream final_file;
    final_file.open(argv[2]);

    string line;
    getline(bundle,line); ///reads first line '#bundle'
    final_file<<line<<endl;
    //final_file<<"trash    "<<endl;///added this because later while adding the total cameras this can be used as a trash line.

    int N,M; ///N is the total number of cameras, M is the number of points
    getline(bundle,line); ///reads second line containing N and M
    istringstream iss(line);
    string word;
    iss>>word;
        stringstream num1(word);
        num1>>N;
    iss>>word;
        stringstream num2(word);
        num2>>M;

    if(N==0)
        error("Error:: Wrong file name as input or no cameras in the file.");

    cout<<"intial number of cameras = "<<N<<endl<<"initial number of points = "<<M<<endl;

    float lower_percent = stof(argv[3])/100.0f;

    if(lower_percent * N < 5.0)
    {
        lower_percent =(500/N)/100.0;
        cerr<<"Warning:: Total expected cameras Very Less. Changing minimum expected cameras to "<<int(lower_percent*N)<<endl;
    }

    float upper_percent = lower_percent + 0.05; //percent range of number of cameras wanted

    //cout<<lower_percent<<" "<<upper_percent<<endl;
    float range = radius_range;
    float epsilon = increment_in_radius_range;
    bool required_range = false;

    int final_cams=N,final_points;
    int count = 0;
    
    unordered_map<int,int> id;
    //select_cam();
    int new_finalcams = final_cams;
    int prev_finalcams = 0;

   
    if(upper_percent>=1.0)
    {
        bundle.close();
        bundle.open(argv[1]);
        getline(bundle,line);

        while(!bundle.eof())
        {
            getline(bundle,line); //reading from file object 'a'
            //cout<<ch;
            final_file<<line<<endl;
        }
        cout<<"final number of cameras = "<<N<<endl<<"final number of points = "<<M<<endl;
        string s = argv[4];
        s += '/';
        //cout<<s<<endl;    
        
        ofstream listoutfile;
        listoutfile.open(argv[5]);
        for(int i = 0; i<N;i++)
        {
            // if(index[i]!=nil)
            {
                listoutfile<<s<<setfill('0') <<setw(4)<<i<<".png"<<endl;
            }
        }
            
    }
    else
    {
        do
        {
            if(range > 1e5 || range < -1e5)
            {
                range = radius_range;
                angle_error += increment_in_angle ;

                epsilon = increment_in_radius_range;
            }
            bundle.close();
            bundle.open(argv[1]);
            getline(bundle,line);
            getline(bundle,line);
            
            count++;
            if(final_cams > int(upper_percent * N))
            {
                if(new_finalcams == prev_finalcams)
                    epsilon *= 2.0;
                range = range + epsilon;
            }
            else
            {
                if(new_finalcams == prev_finalcams)
                    epsilon *= 2.0;
                range = range - epsilon;
            }

            prev_finalcams = new_finalcams;
            
            unordered_map<int,int> index;
            vector<Vector3d> position;
            vector<Mat> rot;

            // int n = 4; ///nth camera is to be considered
            //int tot_points;

            select_cam(range, index, position, rot, N, bundle, final_file, required_range);
            

            //counting total_cameras;
            vector<int> total_cams;

            
            for(int i = 0; i<N;i++)
                total_cams.push_back(index[i]);

            sort(total_cams.begin(),total_cams.end());

            final_cams = total_cams[total_cams.size()-1]+1;
            new_finalcams = final_cams;
            
            id = index;
            //cout<<count<<" range = "<<range<<", final cameras = "<<final_cams<<endl;
        }while(final_cams > int(upper_percent * N) || final_cams<int(lower_percent*N));
        
        int tot_points;
        tot_points = select_points(N,M,id, bundle,final_file,required_range);
        final_file<<final_cams<<" "<<tot_points<<endl;


        required_range=true;

        bundle.close();
        bundle.open(argv[1]);

        getline(bundle,line); ///reads first line '#bundle'
        getline(bundle,line); ///reads second line containing N and M

        unordered_map<int,int> index;
        vector<Vector3d> position;
        vector<Mat> rot;
        //int tot_points;        

        select_cam(range, index, position, rot, N, bundle, final_file, required_range);

        //counting total_cameras;
        vector<int> total_cams;

        // ofstream index_file;
        // index_file.open(argv[3]);
        for(int i = 0; i<N;i++)
        {
            total_cams.push_back(index[i]);
            //index_file<<i<<" "<<index[i]<<endl;
            //cout<<i<<" "<<index[i]<<endl;
        }
        //index_file.close();
        sort(total_cams.begin(),total_cams.end());

        tot_points = select_points(N,M,index, bundle,final_file,required_range); 

        final_cams = total_cams[total_cams.size()-1]+1;
        // final_points = tot_points;

        cout<<"final number of cameras = "<<total_cams[total_cams.size()-1]+1<<endl<<"final number of points = "<<tot_points<<endl;
    
        string s = argv[4];
        s += '/';
        //cout<<s<<endl;    
        
        ofstream listoutfile;
        listoutfile.open(argv[5]);
        for(int i = 0; i<N;i++)
        {
            if(index[i]!=nil)
            {
                listoutfile<<s<<setfill('0') <<setw(4)<<i<<".png"<<endl;
            }
        }
    }
    return 0;

}


/* 
        Requirements:
            (unordered_map)index -> maps original index to new index
            vector<cv::Point3d> position -> updated according to new index, i.e. position[new_index] is a R3 vector
            vector<cv::Mat> rot -> read from bundle.out and write here

        IDEA: 
            for all cameras
                read first line = f d1 d2 -> store ina  temporary vector<>f
                read next 3 lines, store in a temporary cv::Mat M
                read next line, store in a temporary vector<cv::Point3d>P
                
                compare_and_update(input camera_num, input P, input M, input position, input rot, output_file final):
                {
                    for all i in vector<>position in reverse order
                    if (P - position[i] < error)
                        if(M-rot[i] > angle_error)
                            update index, update position, update rot
                            write to final;
                            return;
                        else
                            return;
                    update index, update position, update rot;
                    write to file;
                    return;
                }
            for all points
                store px,py,pz
                store RGB
                store length
                int len = 0;
                    for i in length
                        read cam no.
                        if(index[cam no.] exists)
                            store index[cam no.], key, X, Y
                            len++
                        if(len!=0)
                            write in final:
                                px py pz
                                RGB
                                len cam key x y, ...
         */